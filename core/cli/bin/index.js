#! /usr/bin/env node

'use strict';

const importLocal = require('import-local');
const log = require('@x3-cli-dev/log');

if (importLocal(__filename)) {
  log.info("cli", "using local version of x3-cli-dev");
} else {
  require('../lib')(process.argv);
}
