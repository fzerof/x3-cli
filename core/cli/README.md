# `cli`

> @x3-cli-dev/cli

## 介绍

> 基于 X3 项目开发的脚手架，提供了项目创建、发布、SVN 提交、本地插件依赖安装等功能。

## 安装

```js
npm install @x3-cli-dev/cli -g
```

## 全局 Options

```bash
Options:
  -v, --version                             显示版本号
  -d, --debug                               是否启用调试模式
  -tp, --target-path <targetPath>           是否指定本地调试路径
  -h, --help                                显示帮助信息
```

```bash
x3 --version
```

## 命令

### init

> 初始化项目

```bash
Usage: x3 init [options] <projectName>

初始化项目

Options:
  -f, --force          强制覆盖目标目录(如果存在)
  -si, --skip-install  跳过依赖安装
```

```bash
mkdir <projectName>
cd <projectName>
x3 init <projectName>
```

### publish

> 发布项目到远程服务器

```bash
Usage: x3 publish [options]

发布项目

Options:
  -sbu, --skip-build   跳过项目打包
  -sba, --skip-backup  跳过服务器文件备份
```

```bash
x3 publish
```

### svn

> 提交发布项目到 SVN

```bash
Usage: x3 svn [options]

SVN 发布

Options:
  -f, --force              不询问，直接发布
  -m, --message <message>  提交信息
```

```bash
x3 svn -m 'commit message'
```

### install

> 本地插件依赖安装

```bash
Usage: x3 install [options] <source> <destination>

安装本地依赖到本地仓库

Arguments:
  source              要安装的本地依赖 PackageName
  destination         要安装的目的仓库 PackageName

Options:
  -sbu, --skip-build  跳过项目打包
  -spa, --skip-pack   跳过 npm pack
```

```bash
x3 install x3-base-core x3-bills-plugin
```

### config

> 增删改查本地配置信息

```bash
Usage: x3 config [options]

配置增删改查

Options:
  --set <keyValue...>            设置 config, 可同时设置多个, 如 x3 -s key1 value1 key2 value2
  --get <keys...>                查询 config, 可同时查询多个, 如 x3 -g key1 key2 key3
  -del, --delete <keys...>       删除 config, 可同时删除多个, 如 x3 -d key1 key2 key3
  -ls, --list                    查看所有 config
  --edit                         编辑 config, 使用 vim 编辑器
  -lis, --list-server            查看服务器配置列表
  -as, --add-server <server>     添加服务器配置
  -rs, --remove-server <server>  删除服务器配置
  -us, --use-server <server>     使用指定的服务器配置
```

```bash
x3 config [options]

x3 config --list
```

### clean

> 清除本地缓存

```bash
Usage: x3 clean [options]

清除缓存, 默认情况下只清除 command 缓存

Options:
  -f, --force     不询问，强制删除
  -t, --template  清除包含模板缓存
```

```bash
x3 clean -f -t
```

### proxy

> 启动本地代理

```bash
Usage: x3 proxy [options]


启动本地代理

Options:
  -ls, --list [proxy-name]  查看环境代理
  -u, --use <proxy-name>    使用环境代理
  -e, --edit                编辑 proxy, 使用 vim 编辑器
  -p, --port <number>       代理启动端口号，默认 3000
  -h, --help                显示帮助信息
```

```bash
x3 proxy --list
```

## 默认环境变量

- 最低 node 版本 LOWEST_NODE_VERSION: '10.18.0',
- 环境变量配置文件名 X3_CLI_ENV_FILENAME: '.x3-cli-dev-env',
- 环境变量配置文件路径 X3_CLI_HOME_PATH
- cli 目录名 X3_DEFAULT_CLI_HOME: '.x3-cli-dev',
- 启动脚本包名 NPM_NAME: '@x3-cli-dev/cli',
- 日志等级 LOG_LEVEL 默认 'info'，可选 'debug'

## 用户缓存路径

用户主目录/X3_CLI_ENV_FILENAME

## 项目线上配置

[仓库地址](https://gitee.com/fzerof/x3-cli-json-server.git)
