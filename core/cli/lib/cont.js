module.exports = {
  // 最低 node 版本
  LOWEST_NODE_VERSION: '10.18.0',
  // 环境变量配置文件名
  X3_CLI_ENV_FILENAME: '.x3-cli-dev-env',
  // cli 目录名
  X3_DEFAULT_CLI_HOME: '.x3-cli-dev',
  // 启动脚本包名
  NPM_NAME: '@x3-cli-dev/cli',
}
