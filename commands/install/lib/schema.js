const CONF_SCHEMA = [
  {
    key: 'repoRootPath',
    name: '仓库根目录',
    message: '请输入仓库根目录',
    type: 'input',
  }
];

module.exports = {
  CONF_SCHEMA,
};
