const Axios = require('axios');

const { matchStrObj } = require('@x3-cli-dev/utils');

const axios = Axios.create();

function getToken(options) {
  const { loginApi, params, method } = options;
  const url = matchStrObj(loginApi, params);
  return axios.request({ url, method, data: params }).then((response) => {

    return [response.data[0].token, response.data[1]?.token];
  });
}

module.exports = {
  getToken,
};
