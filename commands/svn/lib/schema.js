const CONF_SCHEMA = [
  {
    key: 'svnRootPathName',
    name: 'svn根目录名称',
    message: '请输入svn根目录名称',
    type: 'input',
  },
  {
    key: 'svnRootPathValue',
    name: 'svn根目录地址',
    message: '请输入svn根目录地址',
    type: 'input',
  }
];

const CONF_SCHEMA_U = [
  {
    key: 'svnRootPathValue',
    name: 'svn根目录地址',
    message: '请输入当前使用(-u)的svn根目录地址',
    type: 'input',
  }
];

module.exports = {
  CONF_SCHEMA,
  CONF_SCHEMA_U
};
