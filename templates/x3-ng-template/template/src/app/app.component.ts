import { Component } from '@angular/core';
import { FullscreenService } from 'x3-common-core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(public fullScreenService: FullscreenService) {}
}
