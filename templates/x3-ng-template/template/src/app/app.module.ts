import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { X3CommonCoreModule } from 'x3-common-core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { X3HomeThemeConf, X3ThemeModule } from 'x3-base-core/components/theme';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    X3CommonCoreModule,
    TranslateModule.forRoot( {
        loader: {
            provide: TranslateLoader,
            useFactory: ( http: HttpClient ) => new TranslateHttpLoader( http, './assets/i18n/', '.json' ),
            deps: [ HttpClient ]
        }
    } ),
    X3ThemeModule.forRoot(X3HomeThemeConf),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
