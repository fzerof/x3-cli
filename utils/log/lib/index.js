'use strict';

const log = require('npmlog');

log.level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info';
log.heading = 'x3-cli';
log.headingStyle = { fg: 'blue', bg: 'black' }

log.addLevel('success', 3000, { fg: 'green', bg: 'black' })

module.exports = log;
