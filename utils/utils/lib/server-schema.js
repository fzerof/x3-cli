const CONF_SCHEMA = [
  {
    key: 'serverHost',
    name: '远程服务器host',
    message: '请输入远程服务器host',
    type: 'input',
  },
  {
    key: 'sshPort',
    name: 'ssh协议端口号',
    default: 22,
    message: '请输入ssh协议端口号',
    type: 'input',
  },
  {
    key: 'serverName',
    name: '远程服务器用户名',
    message: '请输入远程服务器用户名',
    type: 'input',
  },
  {
    key: 'serverPassword',
    name: '远程服务器密码',
    message: '请输入远程服务器密码',
    type: 'password',
  },
  {
    key: 'serverRootPath',
    name: '远程服务器web跟路径',
    message: '请输入服务器web跟路径',
    type: 'input',
  },
];

module.exports = {
  CONF_SCHEMA,
};
