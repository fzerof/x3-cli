'use strict';

const Axios = require('axios');

const baseurl = process.env.X3_CLI_BASE_URL || 'https://gitee.com/fzerof/x3-cli-json-server/raw/master';
const axios = Axios.create({
  baseURL: baseurl,
  timeout: 5000,
});

function getTemplateList() {
  return axios.get('/template.json').then(res => {
    return res.data;
  });
}

function getProjectList() {
  return axios.get('/project.json').then(res => {
    return res.data;
  });
}

function getPluginList() {
  return axios.get('/plugin.json').then(res => {
    return res.data;
  });
}

module.exports = {
  getTemplateList,
  getProjectList,
  getPluginList,
};
